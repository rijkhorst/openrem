#######
OpenREM
#######

OpenREM is a Django app to extract, store and export Radiation Exposure
Monitoring related information, primarily from DICOM files.

This is a beta release 0.7.3 which fixes a database issue with migrated databases and fixes some minor interface issues.
Please review the `release notes <http://docs.openrem.org/en/latest/release-0.7.3.html>`_ for details.

Full documentation can be found on Read the Docs: http://docs.openrem.org

**For upgrades**, please look at the `version release notes <http://docs.openrem.org/en/latest/release-0.7.3.html>`_

For fresh installs, please look at the `install docs <http://docs.openrem.org/latest/install.html>`_

Contribution of code, ideas, bug reports documentation is all welcome.
Please feel free to fork the repository and send me pull requests. See
`the website <http://openrem.org/getinvolved>`_ for more information.

There is a developer demo site, which at times has a working demo of recent code, but might not, and 
might be broken. It can be found at http://djp-openremdemo.rhcloud.com/openrem/
